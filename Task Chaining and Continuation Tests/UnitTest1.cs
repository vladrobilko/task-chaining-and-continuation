using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_Chaining_and_Continuation;

namespace Task_Chaining_and_Continuation_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async void CreateArrayOfTenRandomIntegers_ReturnsArrayWithTenElements()
        {
            // pre
            var manager = new TaskArrayManager();

            // Act
            var result = await manager.CreateArrayOfTenRandomIntegersAndPrintToConsole();

            // Ass
            Assert.AreEqual(11, result.Length);
        }
    }
}