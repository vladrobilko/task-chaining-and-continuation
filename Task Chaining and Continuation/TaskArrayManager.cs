﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Task_Chaining_and_Continuation
{
    public class TaskArrayManager
    {
        readonly Random _random = new Random();

        public async Task<int[]> CreateArrayOfTenRandomIntegersAndPrintToConsole()
        {
            return await Task.Run(() =>
            {
                var arr = new int[10];

                var result = arr.Select(x => _random.Next()).ToArray();

                PrintArrayToConsole(result);

                return result;
            });
        }

        public async Task<int[]> MultiplyArrayByRandomNumberAndPrintToConsole(int[] task1)
        {
            PrintArrayToConsole(task1);
            return await Task.Run(() =>
            {
                return task1.Select(x => x * _random.Next()).ToArray();
            });
        }

        public async Task<int[]> SortArrayByAscendingAndPrintToConsole(int[] task2)
        {
            PrintArrayToConsole(task2);
            return await Task.Run(() =>
            {
                return task2.OrderBy(x => x).ToArray();
            });
        }

        public Task<int> CalculateAverageAndPrintToConsole(int[] task3)
        {
            PrintArrayToConsole(task3);
            return Task.Run(() => (int)task3.Average());
        }

        private void PrintArrayToConsole(int[] arr)
        {
            Console.WriteLine("-----------------------------------");
            foreach (var item in arr)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
