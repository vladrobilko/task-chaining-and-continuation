﻿using System;
using System.Threading.Tasks;

namespace Task_Chaining_and_Continuation
{
    internal class Program
    {
        public static async Task Main()
        {
            var taskManager = new TaskArrayManager();

            var task1 = await taskManager.CreateArrayOfTenRandomIntegersAndPrintToConsole();

            var task2 = await taskManager.MultiplyArrayByRandomNumberAndPrintToConsole(task1);

            var task3 = await taskManager.SortArrayByAscendingAndPrintToConsole(task2);

            var task4 = taskManager.CalculateAverageAndPrintToConsole(task3);

            await task4.ContinueWith(a => Console.WriteLine($"\nAverage: {a.Result}"));

            Console.Read();
        }
    }
}
